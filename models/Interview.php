<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%interview}}".
 *
 * @property string $name
 * @property string $gender
 * @property string $email
 * @property string $phone
 * @property integer $country
 * @property string $address
 * @property string $answer
 */
class Interview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%interview}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'country', 'address'], 'required'],
            [['gender', 'answer'], 'string'],
            [['country'], 'integer'],
            ['email','email'],
            ['email','checkUniqEmail'],
            ['email','checkUniqPhone'],
            ['gender', 'in', 'range' => ['f','m']],
            [['name', 'email', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20]
        ];
    }
    /**
     * Validor for uniq email adress for value fron redis
     * @param $attribute string
     * @param $params array
     */
    public function checkUniqEmail($attribute, $params)
    {
        if(Hash::exist(['mail' => $attribute])){
            $this->addError($attribute,'User with this email already answer the interview.');
        }
    }
    /**
     * Validor for uniq phone value from redis
     * @param $attribute string
     * @param $params array
     */
    public function checkUniqPhone($attribute, $params)
    {
        if(Hash::exist(['phone' => $attribute])){
            $this->addError($attribute,'User with this phone already answer the interview.');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'gender' => 'Gender',
            'email' => 'Email',
            'phone' => 'Phone',
            'country' => 'Country',
            'address' => 'Address',
            'answer' => 'Answer',
        ];
    }
}
