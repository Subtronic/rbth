<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%interview}}".
 *
 * @property int $id
 * @property string $name
 */
class Country extends \yii\db\ActiveRecord
{
	public static $key = 'CountryList';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getList()
    {
    	if(Yii::$app->cache->get(self::$key) == false){
    		$return = ArrayHelper::map(self::find()->asArray()->all(),'id','name');
    		Yii::$app->cache->set(self::$key,$return);
    	} else {
    		$return = Yii::$app->cache->get(self::$key);
    	}
    	return $return;
    }
}