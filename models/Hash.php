<?php
namespace app\models;

use Yii;

class Hash extends \yii\redis\ActiveRecord
{
	public function attributes()
	{
		return ['id','hash','email','phone'];
	}
	/**
	 * Check existing hash or email in saved entry
	 * Example: $params = ['hash' => 'some value' ...]
	 * @param $params - array()
	 * @return boolean
	 */
	public static function exist($params = null)
	{
		$uniq = true;
		foreach (['hash','mail','phone'] as $attr) {
			if(isset($params[$attr])){
				$uniq = !empty(self::find()->where([$attr => $params[$attr]])->all());
				if(!$uniq) break;
			}
		}
		
		return $uniq && !empty($params); 
	}

}