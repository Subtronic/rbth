<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Cookie;
use app\models\Interview;
use app\models\Hash;
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionInterview(){
        var_dump(Hash::exist());
        $model = new \app\models\Interview();
        if($model->load(Yii::$app->request->post())){
            $hash = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
            if(!isset(Yii::$app->request->cookies['interview']) && !Hash::exist(['hash' => $hash])){
                if($model->save()){
                    $model->loadDefaultValues();
                    $h = new Hash;
                    $h->hash = $hash;
                    $h->phone = $model->phone;
		    $h->email = $model->email;
		    // load don't work with Redis Active record
		    //$h->load(['hash'=>$hash,'phone'=>$model->phone,'mail'=>$model->email]);
		    $h->save();
                    Yii::$app->response->cookies->add( new Cookie([
                        'name'=>'interview',
                        'value'=> true
                        ])
                    );
                    Yii::$app->session->setFlash('contactFormSubmitted');
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('errorSubmit');
                }
            } else {
                return $this->render('interviewForm', ['model' => new Interview()]);
            }
        }
        
        return $this->render('interviewForm', ['model' => $model]);
    }
}
