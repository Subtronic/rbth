<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Country;
use yii\bootstrap\Collapse;
use app\models\Hash;

/* @var $this yii\web\View */
$this->title = 'Interview form';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
	<h1><?= Html::encode($this->title) ?></h1>
	<?if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
	    <div class="alert alert-success">
	        Thank you for answer. Your answer will help make our site better.
	    </div>
	<?else:?>
		<?if(isset(Yii::$app->request->cookies['interview']) || Hash::exist(['hash' => md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'])])):?>
			<div class="alert alert-danger">
		        You already submit this intreview.
		    </div>
		<?endif;?>
		<?if(Yii::$app->session->hasFlash('errorSubmit')):?>
			<div class="alert alert-danger">
		        Something wrong with your input data.
		    </div>
		<?endif;?>
		<?=Collapse::widget([
		    'items' => [
		        [
		            'label' => 'Info',
		            'content' => '<p>This interview form implements test task from RBTH.</p>',
		            'contentOptions' => ['class' => 'in']
		        ],
		        [
		            'label' => 'Detail',
		            'content' => '<p>This form using cookies, redis and MyISAM as table engine.</p>
						<p>Stored in cookies pass indicator survey.</p>
						<p>The redis stores the hash of users who have successfully completed the survey.</p>',
		        ],
		    ]
		]);?>
	    <div class="row">
	        <div class="col-lg-5">
				<?$form = ActiveForm::begin([
					'id' => 'interview-form',
					'options' => ['class' => '']
				]); ?>
				<?=$form->errorSummary($model);?>
				<?=$form->field($model, 'name');?>
				<?=$form->field($model, 'gender')->dropDownList(
					['f'=>'Female','m' => 'Male'],
             		['prompt'=>'- Choose your gender -']
             	);?>
				<?=$form->field($model, 'country')->dropDownList(
					Country::getList(),
					['prompt' => '- Choose your country']
				);?>
				<?=$form->field($model, 'email')->input('email');?>
				<?=$form->field($model, 'phone');?>
				<?=$form->field($model, 'address');?>
				<?=$form->field($model, 'answer')->textArea(['rows'=>5]);?>

				<div class="form-group">
				    <div class="col-lg-offset-1 col-lg-11">
				        <?=Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
				    </div>
				</div>
				<?ActiveForm::end() ?>
			</div>
		</div>
	<?endif;?>
</div>